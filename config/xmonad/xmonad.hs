--      ___           ___           ___           ___           ___           ___
--     |\__\         /\__\         /\  \         /\__\         /\  \         /\  \
--     |:|  |       /::|  |       /::\  \       /::|  |       /::\  \       /::\  \
--     |:|  |      /:|:|  |      /:/\:\  \     /:|:|  |      /:/\:\  \     /:/\:\  \
--     |:|__|__   /:/|:|__|__   /:/  \:\  \   /:/|:|  |__   /::\~\:\  \   /:/  \:\__\
-- ____/::::\__\ /:/ |::::\__\ /:/__/ \:\__\ /:/ |:| /\__\ /:/\:\ \:\__\ /:/__/ \:|__|
-- \::::/~~/~    \/__/~~/:/  / \:\  \ /:/  / \/__|:|/:/  / \/__\:\/:/  / \:\  \ /:/  /
--  ~~|:|~~|           /:/  /   \:\  /:/  /      |:/:/  /       \::/  /   \:\  /:/  /
--    |:|  |          /:/  /     \:\/:/  /       |::/  /        /:/  /     \:\/:/  /
--    |:|  |         /:/  /       \::/  /        /:/  /        /:/  /       \::/__/
--     \|__|         \/__/         \/__/         \/__/         \/__/         ~~


--xxxxxxxxxxxxx
--xx Imports xx
--xxxxxxxxxxxxx

import XMonad
import XMonad.Util.EZConfig (additionalKeysP)
import Data.Monoid
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat, doCenterFloat)
import XMonad.Hooks.WindowSwallowing
import XMonad.Layout.Fullscreen
import XMonad.Layout.Spacing
import XMonad.Util.SpawnOnce
import XMonad.Actions.CopyWindow (kill1)
import Data.Maybe (isJust)
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import qualified Data.Map as M
import qualified XMonad.StackSet as W
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Actions.UpdatePointer
import XMonad.Util.Run

--xxxxxxxxxxxxxxxxxxxxxxxxxxxxx
--xx Defining some variables xx
--xxxxxxxxxxxxxxxxxxxxxxxxxxxxx

myTerminal           = "alacritty"
myPrompt             = "dmenu_run"
myBrowser            = "brave"
myFileManager        = "lf"

myFocusFollowsMouse  = True

myBorderWidth        = 2

myModMask            = mod4Mask

myWorkspaces         = ["1", "2", "3", "4"]

myNormalBorderColor  = "#0F1221"
myFocusedBorderColor = "#907D81"

--xxxxxxxxxxxxxxxxx
--xx Keybindings xx
--xxxxxxxxxxxxxxxxx

myKeys :: [(String, X ())]
myKeys =
    -- Restart xmonad
    [ ("M-r", spawn "xmonad --restart")

    -- Open a terminal
    , ("M-<Return>", spawn (myTerminal))

    -- Open a specific program inside a new terminal instance
    , ("M-. s", spawn (myTerminal ++ " -e fish"))
    , ("M-. h", spawn (myTerminal ++ " -e htop"))
    , ("M-. w", spawn (myTerminal ++ " -e nmtui"))
    , ("M-. r", spawn (myTerminal ++ " -e R -q"))
    , ("M-. l", spawn (myTerminal ++ " -e lua"))
    , ("M-. f", spawn (myTerminal ++ " -e " ++ myFileManager))

    -- Open a specific configuration file inside vim
    , ("M-v v", spawn (myTerminal ++ " -e nvim"))
    , ("M-v c", spawn (myTerminal ++ " -e nvim ~/.config/nvim/init.lua"))
    , ("M-v x", spawn (myTerminal ++ " -e nvim ~/.xmonad/xmonad.hs"))
    , ("M-v f", spawn (myTerminal ++ " -e nvim ~/.config/fish/config.fish"))
    , ("M-v z", spawn (myTerminal ++ " -e nvim ~/.zshrc"))
    , ("M-v a", spawn (myTerminal ++ " -e nvim ~/.config/alacritty/alacritty.yml"))
    , ("M-v t", spawn (myTerminal ++ " -e nvim ~/.Xresources"))
    , ("M-v d", spawn (myTerminal ++ " -e nvim ~/dotfiles/config/dmenu/config.h"))
    , ("M-v l", spawn (myTerminal ++ " -e nvim ~/.config/lf/lfrc"))
    , ("M-v e", spawn (myTerminal ++ " -e nvim ~/.config/emacs/init.el"))

    -- Shortcuts to open certain programs
    , ("M-d", spawn (myPrompt))
    , ("M-b", spawn (myBrowser))
    , ("M-f", spawn (myTerminal ++ " -e " ++ myFileManager))
    , ("M-q", spawn "rustyreboot")

    -- Toggle Float
    , ("M-<Space>", withFocused toggleFloat)

    -- Close the currently focused window
    , ("M-c", kill1)

    -- Change focus between split windows
    , ("M-j", windows W.focusDown)
    , ("M-k", windows W.focusUp)
    , ("M-h", windows W.focusDown)
    , ("M-l", windows W.focusUp)

    -- Swap windows
    , ("M-S-j", windows W.swapDown)
    , ("M-S-k", windows W.swapUp)
    , ("M-S-h", windows W.swapDown)
    , ("M-S-l", windows W.swapUp)

    -- Change window size
    , ("M-C-h", sendMessage Shrink)
    , ("M-C-l", sendMessage Expand)
    , ("M-C-j", sendMessage Shrink)
    , ("M-C-k", sendMessage Expand)

--    , ("M-z", sendMessage (MT.Toggle NBFULL) >> sendMessage ToggleStruts)

    -- Adjust volume
    , ("M-C-d", spawn "amixer set Master 5%+")
    , ("M-C-a", spawn "amixer set Master 5%-")
    , ("M-C-s", spawn "amixer set Master toggle")
    ]
    where
        toggleFloat w = windows (\s -> if M.member w (W.floating s)
            then W.sink w s
            else (W.float w (W.RationalRect (0.01/3) (0.12/4) (1/2) (3.5/5)) s))

--xxxxxxxxxxxxxxxxxxx
--xx Mousebindings xx
--xxxxxxxxxxxxxxxxxxx

myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))
    ]

--xxxxxxxxxxxx
--xx Layout xx
--xxxxxxxxxxxx

myLayoutHook = tiled
  where
    tiled   = Tall nmaster delta ratio
    nmaster = 1
    ratio   = 1/2
    delta   = 3/100

--xxxxxxxxxxxxxxxxx
--xx Manage-Hook xx
--xxxxxxxxxxxxxxxxx

myManageHook = composeAll
    [ className =? "MPlayer"             --> doFloat
    , className =? "Brave-browser"       --> doShift (myWorkspaces !! 0)
    , className =? "rustyreboot"         --> doCenterFloat
    , isFullscreen                       --> doFullFloat
    , className =? "Gimp"                --> doFloat
    , title     =? "floatOn"             --> doFloat
    , resource  =? "desktop_window"      --> doIgnore
    , resource  =? "kdesktop"            --> doIgnore ]

--xxxxxxxxxxxxxxxxxx
--xx StartUp-Hook xx
--xxxxxxxxxxxxxxxxxx

myStartupHook :: X ()
myStartupHook = do
    spawnOnce "$HOME/.xmonad/autostart.sh"

main = do
    xmproc <- spawnPipe "xmobar ~/.xmonad/xmobarrc.hs"
    xmonad $ fullscreenSupportBorder $ def {
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        borderWidth        = myBorderWidth,
        handleEventHook    = handleEventHook def <+> swallowEventHook (className =? "Alacritty" <||> className =? "St" <||> className =? "XTerm") (return True),
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,
        mouseBindings      = myMouseBindings,
        manageHook         = myManageHook,
        startupHook        = myStartupHook,
        layoutHook         = spacingRaw False (Border 25 10 0 10) True (Border 10 0 10 0) True $ myLayoutHook,
        logHook            = dynamicLogWithPP xmobarPP {
            ppCurrent = xmobarColor "#907D81" "" . wrap "[" "]"
            , ppHidden = xmobarColor "#DAC4A6" ""
            , ppHiddenNoWindows = xmobarColor "#DAC4A6" ""
            , ppTitle = xmobarColor "#907D81" "" . shorten 50
            , ppSep = "   "
            , ppOutput = hPutStrLn xmproc
            } >> updatePointer (0.75, 0.75) (0.75, 0.75)
    } `additionalKeysP` myKeys
