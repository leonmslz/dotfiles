--      ___           ___           ___           ___           ___           ___
--     |\__\         /\__\         /\  \         /\  \         /\  \         /\  \
--     |:|  |       /::|  |       /::\  \       /::\  \       /::\  \       /::\  \
--     |:|  |      /:|:|  |      /:/\:\  \     /:/\:\  \     /:/\:\  \     /:/\:\  \
--     |:|__|__   /:/|:|__|__   /:/  \:\  \   /::\~\:\__\   /::\~\:\  \   /::\~\:\  \
-- ____/::::\__\ /:/ |::::\__\ /:/__/ \:\__\ /:/\:\ \:|__| /:/\:\ \:\__\ /:/\:\ \:\__\
-- \::::/~~/~    \/__/~~/:/  / \:\  \ /:/  / \:\~\:\/:/  / \/__\:\/:/  / \/_|::\/:/  /
--  ~~|:|~~|           /:/  /   \:\  /:/  /   \:\ \::/  /       \::/  /     |:|::/  /
--    |:|  |          /:/  /     \:\/:/  /     \:\/:/  /        /:/  /      |:|\/__/
--    |:|  |         /:/  /       \::/  /       \::/__/        /:/  /       |:|  |
--     \|__|         \/__/         \/__/         ~~            \/__/         \|__|

-- Dependencies:
-- otf-font-awesome

Config {
       font = "xft:Zekton:size=12:bold:antialias=true"
       , additionalFonts = [ "xft:Font Awesome 6 Free Solid:pixelsize=12"
                           , "xft:Font Awesome 6 Brands:pixelsize=12" ]
       , allDesktops = True
       , bgColor = "#0F1221"
       , fgColor = "#F2DDCE"
       , commands = [ Run Com "echo" ["<fn=1>\xf133</fn>"] "date-icon" 36000
                    , Run Com "/usr/bin/scripts/statusbar/sb-date" [] "date" 600
                    , Run Com "echo" ["<fn=1>\xf017</fn>"] "time-icon" 36000
                    , Run Com "/usr/bin/scripts/statusbar/sb-time" [] "time" 600
                    , Run Com "echo" ["<fn=1>\xf028</fn>"] "volume-icon" 36000
                    , Run Com "/usr/bin/scripts/statusbar/sb-volume" [] "volume" 10
                    , Run Com "echo" ["<fn=1>\xf253</fn>"] "uptime-icon" 36000
                    , Run Com "/usr/bin/scripts/statusbar/sb-uptime" [] "uptime" 600
                    , Run Com "echo" ["<fn=1>\xf1eb</fn>"] "net-icon" 36000
                    , Run Com "/usr/bin/scripts/statusbar/sb-net" [] "net" 600
                    , Run Com "echo" ["<fn=1>\xf2cb</fn>"] "temp-icon" 36000
                    , Run Com "/usr/bin/scripts/statusbar/sb-temp" [] "temp" 30
                    , Run Com "echo" ["<fn=1>\xf019</fn>"] "packages-icon" 36000
                    , Run Com "/usr/bin/scripts/statusbar/sb-packages" [] "packages" 36000
                    , Run Com "echo" ["<fn=1>\xf021</fn>"] "updates-icon" 36000
                    , Run Com "/usr/bin/scripts/statusbar/sb-updates" [] "updates" 36000
                    , Run Com "echo" ["<fn=1>\xf0c7</fn>"] "disk-icon" 36000
                    , Run Com "/usr/bin/scripts/statusbar/sb-disk" [] "disk" 36000
                    , Run StdinReader
                    ]
       , sepChar = "%"
       , alignSep = "}{"
       , template = "%StdinReader% }{  %packages-icon% %packages%  %updates-icon% %updates%  %disk-icon% %disk%  %temp-icon% %temp%   %volume-icon% %volume%   %uptime-icon% %uptime%   %net-icon% %net%   %date-icon% %date%  %time-icon% %time%  "
       }
