-- init.lua
-- Source config files

require("config.options")
require("config.keybindings")
require("config.colorscheme")
require("config.plugins")
