-- plugins.lua
-- Plugin Managment

return require("packer").startup(function(use)

    -- Packer can manage itself
    use "wbthomason/packer.nvim"

    -- Everforest Colorscheme
    use "sainnhe/everforest"

    -- Telescope
    use {
        "nvim-telescope/telescope.nvim", 
        tag = "0.1.1",
        requires = { 
            { "nvim-lua/plenary.nvim" } 
        },
        config = require("telescope").setup({
            pickers = {
                find_files = {
                    theme = "dropdown",
                },
                buffers = {
                    theme = "dropdown",
                },
            },
        }),
    }

    -- Lualine
    use {
        "nvim-lualine/lualine.nvim",
        requires = {
            { "kyazdani42/nvim-web-devicons" }
        },
        config = require("lualine").setup({
            options = {
                theme = "everforest"
            }
        }),
    }

end)
