-- keybindings.lua
-- Custom keybindings

-- Set mapleader to space
vim.g.mapleader = " "

local map = function(mode)
    return function(lhs, rhs)
        vim.keymap.set(mode, lhs, rhs, { noremap = true, silent = true })
    end
end

local N = map("n")
local I = map("i")
local V = map("v")
local O = map("o")

-- Jump to the beginning or end of a line
N("<C-a>", "0")
N("<C-e>", "$")
I("<C-a>", "0")
I("<C-e>", "$")
V("<C-a>", "0")
V("<C-e>", "$")
O("<C-a>", "0")
O("<C-e>", "$")

-- Remove current line
I("<C-k>", "<Esc>0\"_Di")

-- Remap undo
N("U", "<C-r>")

-- Keybindings to move a single line up or down
N("J", ":m .+1<CR>")
N("K", ":m .-2<CR>")

-- Indent lines
V("<", "<gv")
V(">", ">gv")

-- Move between splits
N("<C-h>", "<C-w>h")
N("<C-j>", "<C-w>j")
N("<C-k>", "<C-w>k")
N("<C-l>", "<C-w>l")

-- Move between buffers
N("<Leader>l", ":bn<CR>")
N("<Leader>h", ":bp<CR>")

-- Close focused buffer
N("<C-x>", ":x<CR>")

-- Telescope
local builtin = require("telescope.builtin")
local utils   = require("telescope.utils")

-- Open file picker
N("<Leader>f", function() 
    builtin.find_files({
        hidden    = true,
        no_ignore = true,
        cwd       = utils.buffer_dir(),
    }) 
end)

-- Open file picker showing all files
N("<Leader>a", function() 
    builtin.find_files({
        hidden    = true,
        no_ignore = true,
    }) 
end)

-- Open buffer picker
N("<Leader>b", function() 
    builtin.buffers() 
end)
