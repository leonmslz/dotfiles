-- options.lua
-- Basic Settings

local options = {
    -- Enable cursorline
    cursorline     = true,

    -- Set 1 tab equal to 4 spaces
    tabstop        = 4,
    softtabstop    = 4,
    shiftwidth     = 4,

    -- Use spaces instead of tabs
    expandtab      = true,

    -- Enable relative line numbers
    number         = true,
    relativenumber = true,

    -- Reverse split settings
    splitbelow     = true,
    splitright     = true,

    -- Keep 4 lines visible above and below the cursor
    scrolloff      = 4,

    -- Enable global statusbar
    laststatus     = 3,

    -- Show matching brackets, quotes, etc.
    showmatch      = true,

    -- Disable showmode
    showmode       = false,

    -- Smart search
    ignorecase     = true,
    smartcase      = true,

    -- Start searching immediately
    incsearch      = true,

    -- No wrap, auto backups, swap
    wrap           = false,
    backup         = false,
    swapfile       = false,

    -- Use system clipboard
    clipboard      = "unnamedplus"
}

for option, value in pairs(options) do
    vim.opt[option] = value
end
